import React, { useState } from "react";
import { useAddtoList } from "./ActionsHooks/useAction";

export const AddListItem = () => {
  const [formdata, setFormdata] = useState<string>("");
  const addToList = useAddtoList({ list: formdata });
  const yaziekle = (addText: string) => {
    addToList(addText);
  };
  return (
    <div>
      <label htmlFor="text">Add list item</label>
      <input
        type="text"
        onChange={(event) => setFormdata(event.target.value)}
      />
      <button onClick={() => yaziekle(formdata)}>ADD</button>
    </div>
  );
};
