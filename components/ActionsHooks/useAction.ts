import { useContext } from "react";
import { ListContext } from "../../store/Context";

type Props = {
  list: string;
};

export const useAddtoList = (props: Props) => {
  const context = useContext(ListContext);

  return (payload: string) =>
    context.dispatch({ type: "ADD", payload: props.list });
};
