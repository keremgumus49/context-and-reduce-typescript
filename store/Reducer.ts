export type Actions = {
  type: "ADD" | null;
  payload: string;
};
export const defaultStateS = {
  list: [],
};

export interface defaultState {
  list: string[];
}

export const Listreducer = (
  state: defaultState = defaultStateS,
  action: Actions
): defaultState => {
  switch (action.type) {
    case "ADD":
      return { ...state, list: [...state.list, action.payload] };
    default:
      return state;
  }
};
