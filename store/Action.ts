import { useContext } from "react";
import { ListContext } from "./Context";

export const yaziekleAct = (metin: string) => {
  const { dispatch } = useContext(ListContext);
  dispatch({ type: "ADD", payload: metin });
};
