import { createContext } from "react";
import { defaultState, defaultStateS, Actions } from "./Reducer";

export const ListContext = createContext<{
  state: defaultState;
  dispatch: (action: Actions) => void;
}>({ state: defaultStateS, dispatch: () => {} });
