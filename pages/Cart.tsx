import { useContext } from "react";
import Link from "next/link";
import { ListContext } from "../store/Context";
import styles from "../styles/Home.module.css";
const Sepet = () => {
  const { state } = useContext(ListContext);
  return (
    <div>
      <div className={styles.container}>
        <div>Look the List</div>
        <ul>
          {state.list.map((item, index) => (
            <li key={index}>{`${item}`}</li>
          ))}
        </ul>
        <Link href="/">
          <button> Go to MainPage </button>
        </Link>
      </div>
    </div>
  );
};

export default Sepet;
