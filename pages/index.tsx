import styles from "../styles/Home.module.css";
import Link from "next/dist/client/link";
import { AddListItem } from "../components/yaziekle";

const Home = () => {
  return (
    <div className={styles.container}>
      <AddListItem />
      <Link href="/Cart">
        <button>Click and look to store</button>
      </Link>
    </div>
  );
};
export default Home;
