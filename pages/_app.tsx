import "../styles/globals.css";
import type { AppProps } from "next/app";
import { ListContext } from "../store/Context";
import { Listreducer, defaultStateS } from "../store/Reducer";
import { useReducer } from "react";

function MyApp({ Component, pageProps }: AppProps) {
  const [state, dispatch] = useReducer(Listreducer, defaultStateS);

  return (
    <ListContext.Provider value={{ state, dispatch }}>
      <Component {...pageProps} />
    </ListContext.Provider>
  );
}
export default MyApp;
